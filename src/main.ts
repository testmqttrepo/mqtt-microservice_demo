import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.MQTT,
    options: {
      url: 'mqtt://broker.hivemq.com:1883',
      host: "broker.hivemq.com",
      port: 1883,
      protocol: 'mqtt'
    }
   
});
  await app.listen();
}
bootstrap();
