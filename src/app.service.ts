import { InjectQueue} from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';

@Injectable()
export class AppService {
  constructor(@InjectQueue('firstQueueJob') private audioQueue: Queue) {
 
  }
  getHello(): string {
    return 'Hello World!';
  }

  sendMessage(message){
    console.log('message recived in service')
    message={...message,
      time:1000,
    testid:'A'
    }
   // message.time=1000;
    this.audioQueue.add('message-job',{
      message
    })
    console.log('end of the promise')
  }

}
