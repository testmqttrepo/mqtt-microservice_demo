import { Controller, Get } from '@nestjs/common';
import { Ctx, MessagePattern, MqttContext, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @MessagePattern('/floatinitySystems/float@2021/ems/EM1')
  getTemperature(@Payload() data: string,@Ctx() context: MqttContext) {
    console.log('in controller')
    console.log(`Topic: ${context.getTopic()}`);
    console.log(context.getPacket().payload.toString());
    const message=JSON.parse(context.getPacket().payload)
    this.appService.sendMessage(message)
    //console.log(context.getArgs());
    console.log(`${context.getArgByIndex(0)}`);
  }
}
