import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MessageConsumer } from './message.consumer';

@Module({
  imports: [ ClientsModule.register([
    {
      name: 'MATH_SERVICE',
      transport: Transport.MQTT,
      options: {
        url: 'mqtt://broker.hivemq.com:1883',
      }
    },
  ]),
  BullModule.forRoot({ redis: {
    host: 'localhost',
    port: 6379,
  }}),
  BullModule.registerQueue({
    name: 'firstQueueJob',
  }), 
],
  controllers: [AppController],
  providers: [AppService,MessageConsumer],
})
export class AppModule {}
